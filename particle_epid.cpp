//============================================================================
// Name        : epid0.cpp
// Author      :
// Version     :
// Copyright   : Kaveh Shamsi
// Description : Particle-random-walk epidemiological model
//============================================================================

#include <iostream>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <unistd.h>
#include <math.h>
#include <map>
#include <set>
#include <chrono>
#include <unordered_map>
#include <assert.h>

typedef double real;
typedef int32_t id;

typedef int icor_t;
typedef double rcor_t;

template<typename T>
struct g_point_t {
	T x;
	T y;

	bool operator==(const g_point_t& p2) const {
		return x == p2.x && y == p2.y;
	}

	g_point_t xshifted(T xd) const { return {x + xd, y}; }
	g_point_t yshifted(T yd) const { return {x, y + yd}; }
	g_point_t xyshifted(T xd, T yd) const { return {x + xd, y + yd}; }
};

template<typename T>
bool operator<(const g_point_t<T>& p1, const g_point_t<T>& p2) {
	return (p1.x < p2.x) || (p1.x == p2.x && p1.y < p2.y);
}

template<typename T>
bool operator<=(const g_point_t<T>& p1, const g_point_t<T>& p2) {
	return (p1.x <= p2.x) || (p1.x == p2.x && p1.y <= p2.y);
}

typedef g_point_t<rcor_t> rpoint_t;
typedef g_point_t<icor_t> point_t; // integer point


class screen_t {
public:

	int height = 500;
	int width = 1000;

	enum color_t {RED, GREEN, BLUE, BLACK, WHITE};

	color_t background = BLACK;

	SDL_Window* win = nullptr;
	SDL_Renderer* rend = nullptr;

	struct object {
		point_t cord;
		color_t col = BLUE;
		SDL_Texture* text = nullptr;
	};

	id next_id = 0;
	std::unordered_map<id, object> objmap;


	void blit(SDL_Texture *texture, int x, int y) {
		SDL_Rect dest;

		dest.x = x;
		dest.y = y;
		SDL_QueryTexture(texture, NULL, NULL, &dest.w, &dest.h);
		SDL_RenderCopy(rend, texture, NULL, &dest);
	}

	id add_obj(point_t& p) {
		auto& obj = objmap[next_id];
		obj.cord.x = p.x;
		obj.cord.y = p.y;
		return next_id++;
	}

	void set_render_color(color_t c) {
		switch (c) {
		case BLUE:
			SDL_SetRenderDrawColor(rend, 0, 0, 255, 255);
			break;
		case RED:
			SDL_SetRenderDrawColor(rend, 255, 0, 0, 255);
			break;
		case WHITE:
			SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
			break;
		case BLACK:
			SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);
			break;
		}
	}

	void update() {
		clear_screen();
		for (auto& p : objmap) {
			SDL_Rect rc;
			rc.w = 4;
			rc.h = 4;
			rc.x = p.second.cord.x;
			rc.y = p.second.cord.y;
			set_render_color(p.second.col);
			SDL_RenderFillRect(rend, &rc);
		}
		SDL_RenderPresent(rend);
		set_render_color(background);
	}

	void clear_screen() {
		set_render_color(background);
		SDL_RenderClear(rend);
	}

	screen_t() {
	   if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
			printf("error initializing SDL: %s\n", SDL_GetError());
		}
		win = SDL_CreateWindow("GAME", // creates a window
										   SDL_WINDOWPOS_CENTERED,
										   SDL_WINDOWPOS_CENTERED,
										   width, height, 0);

		// triggers the program that controls
		// your graphics hardware and sets flags
		Uint32 render_flags = SDL_RENDERER_ACCELERATED;

		// creates a renderer to render our images
		rend = SDL_CreateRenderer(win, -1, render_flags);

	}

	void wait_on_events(int64_t mseconds) {

		bool close = false;

		auto start = std::chrono::steady_clock::now();
		// annimation loop
		while ( !close && (mseconds == -1 || std::chrono::steady_clock::now() - start < std::chrono::milliseconds(mseconds)) ) {
			SDL_Event event;

			// Events management
			while (SDL_PollEvent(&event)) {
				switch (event.type) {

				case SDL_QUIT:
					// handling of close button
					close = 1;
					break;
				}
			}
		}

		if (close) {
			this->~screen_t();
			exit(1);
		}

	}



	~screen_t() {
		// destroy texture
		for (auto& p : objmap)
			if (p.second.text)
				SDL_DestroyTexture(p.second.text);
		// destroy renderer
		if (rend)
			SDL_DestroyRenderer(rend);
		// destroy window
		if (win)
			SDL_DestroyWindow(win);
		// close SDL
		SDL_Quit();
	}

};


class simulation_t {

public:
	typedef int64_t id;

	bool random_place = true;

	real time = 0;
	real time_step = 1;

	real infectious_period_start = 2;
	real infectious_period_end = 14;

	real mutation_prob = 1e-6;
	real mobility = 3;

	real vax_infect_efficiency = 0.6;
	real vax_death_efficiency = 0.99;

	real r0_prest = 1.1;
	real r0_vaxed = vax_infect_efficiency * r0_prest;

	id num_nodes = 500;

	id cur_step = 0;

	int infect_distance = 10;

	struct node_t {
		id oid = -1;
		real infected_time = -1;
		real vaxed_time = -1;
		point_t cord = {-1, -1};
	};

	static real get_rand_real(real lo, real hi) {
		return lo + static_cast<float>(rand()) / ( static_cast<float>(RAND_MAX/(hi - lo)) );
	}
	static icor_t get_rand_int(icor_t lo, icor_t hi) {
		return lo + (rand() % (abs(hi - lo) + 1));
	}


	std::vector<node_t> nodevec;

	double antivax_mutation_duration;

	std::map<point_t, std::set<id>> pgridmap;

	screen_t scr;

	void clear_screen() {
		scr.clear_screen();
	}

	void update_screen() {
		scr.update();
	}

	void init() {

		for (id i = 0; i < num_nodes; i++) {
			node_t nd;
			if (random_place) {
				nd.cord.x = get_rand_real(0, 1) * scr.width;
				nd.cord.y = get_rand_real(0, 1) * scr.height;
				std::cout << "init at " << nd.cord.x << " " << nd.cord.y << "\n";
			}
			else {
				assert(false);
			}
			nd.oid = scr.add_obj(nd.cord);
			if (i == 0) {
				nd.infected_time = cur_step;
			}
			add_to_pgridmap(nd.oid, nd.cord);
			nodevec.push_back(nd);
		}

	}

	void add_to_pgridmap(id nid, point_t cord) {
		auto rx = round_to_grid(cord.x, infect_distance);
		auto ry = round_to_grid(cord.y, infect_distance);
		point_t xy = {rx, ry};
		pgridmap[xy].insert(nid);
	}


	template<typename T>
	static T clip_value(T a, T lo, T hi) {
		return std::min(hi, std::max(a, lo));
	}

	void clip_cords(point_t& p) {
		p.x = clip_value(p.x, 0, scr.width);
		p.y = clip_value(p.y, 0, scr.height);
	}

	void round_to_grid() {

	}

	static bool are_near(const point_t& p1, const point_t& p2, int dist) {
		if (abs(p1.x - p2.x) < dist && abs(p1.y - p2.y) < dist) {
			return true;
		}
		return false;
	}

	icor_t round_to_grid(icor_t x, int dist) {
		icor_t m = x % dist;
		if (m < dist/2) {
			return x - m;
		}
		else {
			return x - m + dist;
		}
	}

	void update_grid(id pntd, point_t prev_cord, point_t new_cord) {
		auto rx = round_to_grid(prev_cord.x, infect_distance);
		auto ry = round_to_grid(prev_cord.y, infect_distance);
		auto nrx = round_to_grid(new_cord.x, infect_distance);
		auto nry = round_to_grid(new_cord.y, infect_distance);
		point_t xy = {rx, ry};
		point_t nxy = {nrx, nry};
		auto it = pgridmap.find(xy);
		assert(it != pgridmap.end());
		auto nit = pgridmap.find(nxy);
		if (nit != it) {
			it->second.erase(pntd);
			if (it->second.empty()) {
				pgridmap.erase(it);
			}
			pgridmap[nxy].insert(pntd);
		}
	}

	std::set<id> find_close_points(id pntid, int dist) {
		auto& nd = nodevec.at(pntid);
		std::set<id> ret;
		auto rx = round_to_grid(nd.cord.x, infect_distance);
		auto ry = round_to_grid(nd.cord.y, infect_distance);
		point_t xy = {rx, ry};
		auto it = pgridmap.find(xy);
		if (it != pgridmap.end()) {
			for (auto cpid : it->second) {
				auto& cnd = nodevec.at(cpid);
				if (are_near(cnd.cord, nd.cord, infect_distance)) {
					ret.insert(cpid);
				}
			}
		}
		return ret;
	}

	void collision_procedures(id pntid, point_t& prev_cord, const point_t& new_cord) {
		auto& curnd = nodevec.at(pntid);
		if (curnd.infected_time != -1) {
			auto close_points = find_close_points(pntid, infect_distance);
			for (auto xid : close_points) {
				nodevec.at(xid).infected_time = cur_step;
			}
		}
		update_grid(pntid, prev_cord, new_cord);
	}

	void move_point(node_t& nd) {
		point_t prev_cord = nd.cord;
		nd.cord.x += get_rand_int(-2, 2);
		nd.cord.y += get_rand_int(-2, 2);
		clip_cords(nd.cord);

		collision_procedures(nd.oid, prev_cord, nd.cord);

		auto& sobj = scr.objmap.at(nd.oid);
		sobj.cord = nd.cord;
		sobj.col = (nd.infected_time == -1) ? screen_t::BLUE : screen_t::RED;
	}

	void step() {
		for (id i = 0; i < nodevec.size(); i++) {
			move_point(nodevec[i]);
		}
		cur_step++;
	}

	void print_points() {
		for (auto& nd : nodevec) {
			std::cout << "point " << nd.oid << " at (" << nd.cord.x << " " << nd.cord.y << ")\n";
		}
	}


	void run(uint days) {
		for (uint d = 0; d < days; d++) {
			std::cout << "time " << d << "\n";
			step();
			//print_points();
			update_screen();
			scr.wait_on_events(3);
		}
		scr.wait_on_events(-1);
	}
};


int main(int argc, char *argv[]) {

	simulation_t sim;
	sim.init();
	sim.run(10000);

    return 0;
}
