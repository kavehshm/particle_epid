
## A Random-Walk Particle-based Epidemiological Model with SDL Graphics ##

This is a C++ single file for random-walking particles with proximity detection using a grid technique for infection propagation. It uses SDL for visualization.

You can build by running `make` on a linux system with SDL installed. 

Here is a picture:

![plot](./fig/pic0.jpg)