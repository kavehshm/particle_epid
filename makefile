CC = gcc
FLAGS = -O3 -g0 -fopenmp
CC_FLAGS = $(FLAGS)
CXX = g++-9
CXX_FLAGS = -w $(FLAGS) -std=c++2a 

# Final binary
BIN = particle_epid

# Put all auto generated stuff to this build dir.
BUILD_DIR = ./build
BIN_DIR = ./bin

# List of all .cpp source files.
CPP_FILES = ./particle_epid.cpp

CXX_FLAGS += `sdl2-config --libs --cflags` -lSDL2_image

# All .o files go to build dir.
OBJ += $(CPP_FILES:%.cpp=$(BUILD_DIR)/%.o)
OBJ += $(CC_FILES:%.cc=$(BUILD_DIR)/%.o)
OBJ += $(C_FILES:%.c=$(BUILD_DIR)/%.o)

# Gcc/Clang will create these .d files containing dependencies.
DEP = $(OBJ:%.o=%.d)

# print target
print-%  : ; @echo $* = $($*)

# all target
all : $(BIN_DIR)/$(BIN)

# Actual target of the binary - depends on all .o files.
$(BIN_DIR)/$(BIN) : $(OBJ)
# Create build directories - same structure as sources.
	mkdir -p $(@D)
# Just link all the object files.
	@echo hello
	$(CXX) $(OBJ) $(CXX_FLAGS) -o $@


# Include all .d files
-include $(DEP)

# Build target for every single object file.
# The potential dependency on header files is covered
# by calling `-include $(DEP)`.
	  
$(BUILD_DIR)/%.o : %.cpp
	mkdir -p $(@D)
	$(CXX) $(CXX_FLAGS) -MMD -c $< -o $@

$(BUILD_DIR)/%.o : %.cc
	mkdir -p $(@D)
	$(CXX) $(CXX_FLAGS) -MMD -c $< -o $@

$(BUILD_DIR)/%.o : %.c
	mkdir -p $(@D)
# The -MMD flags additionaly creates a .d file with
# the same name as the .o file.
	$(CC) $(CC_FLAGS) -MMD -c $< -o $@


.PHONY : clean
clean : 
# This should remove all generated files.
	-rm $(BUILD_DIR)/$(BIN) $(OBJ) $(DEP)

